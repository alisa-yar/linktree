# Linktree:sparkles:
This is a free (and easy to use) alternative to linktree 

## Where to host your website?
Github Pages: https://pages.github.com  
Netlify: https://netlify.app

## Demo
https://alisa-yar.github.io/



<!-- You can display an image by adding ! and wrapping the alt text in [ ]. Then wrap the link for the image in parentheses ().
![This is an image](https://myoctocat.com/assets/images/base-octocat.svg) -->

<!-- Inspired from: https://github.com/RyanLefebvre/TREE and https://github.com/johnggli/linktree -->
